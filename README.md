# Scalability TFG thesis

This is the repo where I will store my progress. I will write my graduation thesis as a Pandoc document and export it to PDF / LATEX or whatever.

Bibliography will be handled by Mendeley exports to .bib or something like this. I still have not figured it out 100%.

I will post more about the build system of the thesis. I guess I will do some script or something.

For now, it seems safe to assume that I will have a title and then folders with the different chapters. In each chapter folder, I will have the images, code... relevant to that part.

Basic command it seems I will use to build the thesis:

    pandoc -o thesis.pdf --toc title.md chapter1/chapter1.md chaptern/chaptern.md

And this should be it for now!
