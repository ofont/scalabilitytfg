# Conclusions {.unnumbered}

In this project we have found that latency is the number one cause of slow page load times. It has also been shown how latency can't be reduced from the physical layer, so it's an issue that must be tackled higher in the network stack.

The current network stack has not been built to enable low latency and realtime services, which is where web applications are heading. To solve this dissonance between what we want and what we have, there are a lot of workarounds and fixes in the TCP and HTTP 1.1 protocols. Still, those fixes do not solve the fundamental problems, which will be better resolved in the future version of HTTP 2.0.

Finally, we have performed a performance assessment on a current web application, LdShake. In this performance assessment, we have demonstrated that the acquired knowledge in the previous section is indeed useful, and we have also shown that front-end optimizations can yield an enormous decrease in page load times (as much as 75% in our tests), which will hopefully translate to more successful services.
