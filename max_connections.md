# Max parallel connections per server. Different browsers:

It's seems that 6 is the magic number accross browsers.

[More info from stackoverflow](http://stackoverflow.com/questions/985431/max-parallel-http-connections-in-a-browser):

Max Number of default simultaneous persistent connections per server/proxy:

    Firefox 2:  2
    Firefox 3+: 6
    Opera 9.26: 4
    Opera 12:   6
    Safari 3:   4
    Safari 5:   6
    IE 7:       2
    IE 8:       6
    IE 10:      8
    Chrome:     6

Apparently, the HTTP 1.1 RFC says to limit persistent connections to 2 per server.

Extracted from the [RFC 2616](http://tools.ietf.org/html/rfc2616#section-8.1.4) (p.46):

> Clients that use persistent connections SHOULD limit the number of simultaneous connections that they maintain to a given server. A single-user client SHOULD NOT maintain more than 2 connections with any server or proxy. A proxy SHOULD use up to 2*N connections to another server or proxy, where N is the number of simultaneously active users. These guidelines are intended to improve HTTP response times and avoid congestion.

In [RFC 2616, section 8.1.1](http://tools.ietf.org/html/rfc2616#section-8.1.1), the reasons to use persistent connections, and to use them by default are detailed (in contrast to per URL connection of HTTP 1.0):

> 8.1.1 Purpose
> 
> Prior to persistent connections, a separate TCP connection was established to fetch each URL, increasing the load on HTTP servers and causing congestion on the Internet. The use of inline images and other associated data often require a client to make multiple requests of the same server in a short amount of time. Analysis of these performance problems and results from a prototype implementation are available [26] [30]. Implementation experience and measurements of actual HTTP/1.1 (RFC 2068) implementations show good results [39]. Alternatives have also been explored, for example, T/TCP [27].
> 
> Persistent HTTP connections have a number of advantages:
> 
> - By opening and closing fewer TCP connections, CPU time is saved in routers and hosts (clients, servers, proxies, gateways, tunnels, or caches), and memory used for TCP protocol control blocks can be saved in hosts.
> 
> - HTTP requests and responses can be pipelined on a connection. Pipelining allows a client to make multiple requests without waiting for each response, allowing a single TCP connection to be used much more efficiently, with much lower elapsed time.
> 
> - Network congestion is reduced by reducing the number of packets caused by TCP opens, and by allowing TCP sufficient time to determine the congestion state of the network.
> 
> - Latency on subsequent requests is reduced since there is no time spent in TCP's connection opening handshake.
> 
> - HTTP can evolve more gracefully, since errors can be reported without the penalty of closing the TCP connection. Clients using future versions of HTTP might optimistically try a new feature, but if communicating with an older server, retry with old semantics after an error is reported.
> 
> HTTP implementations SHOULD implement persistent connections.

Also, an [analysis](http://www.ibiblio.org/mdma-release/http-prob.html) of the shortcomings of HTTP 1.0 (and HTTP in general) and why it interacts poorly with TCP.

Related to the previous link, an [analysis](http://bitsup.blogspot.com.es/2011/09/spdy-what-i-like-about-you.html) of why SPDY is better (performance wise) than HTTP.

## Chrome
It's 6. [Source](https://code.google.com/p/chromium/issues/detail?id=12066)

## Firefox
network.http.max-connections

:   The maximum number of connections that Firefox will open at the same time.

network.http.max-connections-per-server

:   The number of open connections that Firefox opens to a single server.

network.http.max-persistent-connections-per-proxy

:   The maximum number of persistent connections to a proxy server.

network.http.max-persistent-connections-per-server

:   The maximum number of persistent ("keep-alive") connections to a single server

[Reference](http://stackoverflow.com/questions/1848072/what-are-the-meanings-of-those-connection-limit-configurations-for-firefox)

### Current values, as of Firefox 16 (Mac OSX)
    network.http.max-connections 256
    network.http.max-connections-per-server 15
    network.http.max-persistent-connections-per-proxy 8
    network.http.max-persistent-connections-per-server 6

## Opera & Safari & Mobile
Maybe later...
