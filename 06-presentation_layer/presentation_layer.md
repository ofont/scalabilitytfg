# Presentation Layer
To achieve the best *perceived* performance from the client, the rendering of the page should be progressive. To aid the browser in this matter, it is important to understand how it constructs the parse tree, in order to deliver key resources as soon as possible.

![Browser processing pipeline, from @Grigorik2013, ch. 10](doc-render-js.png)

The browser processing pipeline builds the Document Object Model (DOM) out of the HTML file and a lesser known CSS Object Model (CSSOM). Once we have those objects models, we can build a rendering tree and start painting the webpage.

This seemingly simple schema starts to get confusing when javascript is introduced. Javascript can modify both the DOM and the CSSOM, which complicates the dependency graph and may cause quite a performance hit in our page load times.

We have to be especially aware of `document.write` calls, since they are synchronous and block DOM parsing and construction. We also have to be aware of javascript calls that query the CSSOM. If we perform one of those, the query can't be answered until the CSSOM is available, and while the query is waiting, so it is the construction of the DOM.

Since the CSSOM is so important, we have to deliver CSS resources as soon as possible. To ensure that this will be the default scheduling behaviour of the browser, it is strongly recommended to **put the stylesheets at the top** of the HTML page.

Scripts block the rendering of the content placed below themselves until they have finished running, so it is strongly recommended to **put scripts at the bottom** of the HTML page.

## 3rd party snippets
A lot of websites use 3rd party scripts to add features like analytics, twitter or facebook integration, etc. Those scripts are typically retrieved from the 3rd party site each time the page loads, since it makes it easier for that 3rd party to deploy updates seamlessly. The fact that we are retrieving a resource from this 3rd party URL might introduce a single point of failure in our webpage, if we are not careful.

As we have said before, scripts block all elements below themselves, so if the snippet loads synchronously and the service is overloaded or unavailable, our page may take a very long time to load. Moreover, if this script is placed on the top, we will be staring at a blank page until a response is received or the connection times out (which can take as long as 120 seconds on Internet Explorer).

Now that we are aware that 3rd party snippets should be loaded asynchronously, we have to be able to distinguish between async or synchronous scripts. Here are 2 examples:

    <script type="text/javascript" src="https://platform.
    twitter.com/anywhere.js"></script>

This is a synchronous script. Basically, any script with a *src* attribute will freeze until the resource pointed by the uri has been downloaded.

    <script type="text/javascript">
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-XXXXX-X']);
      _gaq.push(['_trackPageview']);

      (function() {
        var ga = document.createElement('script'); ga.type = 
        'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 
        'https://ssl' : 'http://www') + '.google-analytics.
        com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.
        parentNode.insertBefore(ga, s);
      })();
    </script>

This Google Analytics script is loaded asynchronously. It creates a `<script>` element, pointing to the 3rd party script, sets the *async* property to true and injects the new DOM object in the webpage.

The *async* property used was introduced in HTML5 and it basically tells the browser to execute the script contents as soon as they are available without blocking.

To transform a synchronous snippet to async, we can simply add the async keyword like this:

    <script async type="text/javascript" src="https://platform.
    twitter.com/anywhere.js"></script>

This `async` atttribute is especially useful to prevent external issues to bring our page down. We can also use it for our own scripts, although we'll have to be careful: `async` will trigger the script as soon as it is available. That means that if the script depends on a library that has yet to be loaded, its execution will fail. To allow asynchronous script execution, while maintaining the order in which they are executed, the `defer` keyword was also added. `defer` loads scripts asynchronously but guarantees that they will be executed in the order they ocurr in the page. More information in that regard can be found on @Gentilcore.

## Summary
In this chapter we remark how a careless positioning of the stylesheets and javascript elements in the HTML may increase the rendering time of the webpage.

Even though modern browsers do much more to decrease webpage load times, like prefetching the DNS of our most visited websites, or automatically initiating a handshake with our top ranked pages on startup, the tricks mentioned in this chapter are the most relevant from the perspective of a web application developer.

## Chapter references
The content on this chapter is based on @Grigorika ch. 10, and @Souders2007 ch. 5 and 6. The 3rd party snippets section is based on a presentation by Steve Souders at Fluent Conference 2012 (@Souders).
