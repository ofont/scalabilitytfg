# Network layer

In this chapter I will discuss how bandwidth and latency affect the performance of a webpage.

## More bandwidth, faster surfing? 
<!--
    Explain here that better connections do not necessarily mean better performance.

    Based on this document from Mile Belshe (Google):

    https://docs.google.com/a/chromium.org/viewer?a=v&pid=sites&srcid=Y2hyb21pdW0ub3JnfGRldnxneDoxMzcyOWI1N2I4YzI3NzE2
-->

Even though it may seem that the key factor to speed up web surfing should be bandwidth, it has been proven that it does not matter that much (@Belshec). A typical webpage instantiates multiple HTTP connections to transmit small resources, so the TCP connections tend to be short and bursty, which is not what TCP is optimized for. This means that the bottleneck when delivering webpages is basically a matter of latency.

Improving latency, in contrast to improving bandwidth capacity, is a very hard thing to do. Latency is limited by the propagation speed of the signal on the wire, and those speeds are already pretty close to the speed of light, so the only way it could be reduced is by deploying shorter links.

Latency may be theoretically limited by the distance between a pair of endpoints, but in practice, the connection's last mile is also frequent bottleneck. The technology employed by the user's connection will greatly influence the round trip time. The variance can be as high as 2 orders of magnitude from the best connections to the worst(fibre vs GPRS).

In the following paragraphs, I will show a series of tests performed on @Belshec where we can clearly identify this bandwidth bottleneck. I will also end the chapter talking about which kind of connections we can expect from our users (relevant to perform realistic performance tests on our websites) and how mobile connections exacerbate this latency issue.

## Test: Vary the bandwidth
In this test a fixed RTT value is used (60ms), while the bandwidth has been increased. The test has been performed with 25 pages of the Alexa top.

| Bandwidth | Page Load Time via HTTP |
| --------- | ----------------------- |
| 1Mbps     | 3106ms                  |
| 2Mbps     | 1950ms                  |
| 3Mbps     | 1632ms                  |
| 4Mbps     | 1496ms                  |
| 5Mbps     | 1443ms                  |
| 6Mbps     | 1406ms                  |
| 7Mbps     | 1388ms                  |
| 8Mbps     | 1379ms                  |
| 9Mbps     | 1368ms                  |
| 10Mbps    | 1360ms                  |

: PLT vs Bandwidth

![Page load time vs bandwidth, from @Belshec](plt_vs_bandwidth.png)

![Bandwidth percentage improvement, from @Belshec](benefit_adding_bandwidth.png)

![Effective bandwidth usage, from @Belshec](effective_bandwidth_http.png)

As we can see, once we have hit the 5~6Mbps mark, we get diminishing returns for every added megabit. It is appalling that a 10Mbps connection can only use 1.6Mbps of effective bandwidth to download a page.

## Test: Vary the RTT
In this test the bandwidth is fixed at 5Mbps and the RTT varied from 240ms to 0ms.

|  RTT  | Page Load Time via HTTP |
| ----- | ----------------------- |
| 240ms | 3964ms                  |
| 220ms | 3707ms                  |
| 200ms | 3418ms                  |
| 180ms | 3151ms                  |
| 160ms | 2874ms                  |
| 140ms | 2572ms                  |
| 120ms | 2291ms                  |
| 100ms | 2017ms                  |
| 80ms  | 1732ms                  |
| 60ms  | 1450ms                  |
| 40ms  | 1201ms                  |
| 20ms  | 981ms                   |
| 0ms   | 814ms                   |

: PLT vs RTT

![Page load time vs RTT, from @Belshec](plt_vs_rtt.png)

![RTT percentage improvement, from @Belshec](benefit_reduced_rtt.png)

![Effective bandwidth usage, from @Belshec](effective_bandwidth_rtt.png)

We can see that reducing the RTT gives us a linear increase in performance, in contrast to increasing bandwidth. It is still surprising to see that, even with and RTT of 0, we can only use about half the available bandwidth (2.5Mbps) to transmit the web. This is certainly an improvement over the 1.5Mbps available with an RTT of 60ms, but very far from the full capacity of the link. This performance *loss* is due to the way the transport and application layer work, which we will discuss in the next chapters. The browser renderer scheduler also affects the perceived performance of the page, which is another aspect we will discuss.


## Latency in perspective
To put the latency issue in perspective, I will extract some data from the periodical broadband reports issued by the FCC in the US. Specifically, I will refer to the February 2013 (@FCC), the July 2012 (@FCC20) and the August 2011 (@FCCa) reports.

| technology | february 2013 | july 2012 | august 2011 |
| ---------- | ------------- | --------- | ----------- |
| fiber      | 18ms          | 18ms      | 17ms        |
| DSL        | 44ms          | 43ms      | 44ms        |
| Cable      | 26ms          | 26ms      | 28ms        |

: Latency broadband USA

As we can see, those numbers remain basically unchanged in 2.5 years, while the average subscribed speed has gone from 11.1Mbps (August 2011) to 15.6Mbps (february 2013). Those are US numbers, but they are a good reflection of the overall tendency.

![Average Peak Period Latency in Milliseconds by Technology—September 2012 Test Data](us_latency_chart.jpg)

This chart is also interesting as we can see how bandwidth influences latency very little. Only DSL shows any kind of significant variance, and it can still be counted among the 10s of milliseconds.

In the mobile space, think are much worse. If we take data from the @Gregorik2012:

> Users of the Sprint **4G network** can expect to experience average speeds of 3Mbps to 6Mbps download and up to 1.5Mbps upload with an **average latency of 150ms**. On the Sprint **3G network**, users can expect to experience average speeds of 600Kbps - 1.4Mbps download and 350Kbps - 500Kbps upload with an **average latency of 400ms**.

So mobile has very, very bad latency. And mobile is key. ("We talk about 'mobile first' in 2012, but we want to be 'mobile best' in 2013" (@Kanalley) as stated by Facebook Vice President of Partnerships Dan Rose).

Putting some worldwide numbers on latency:

Avg RTT to Google in 2012 (@Grigorik2012b):

- Worldwide: 100ms
- US: ~50-60ms

|    Country    | Mobile-only users |
| ------------- | ----------------- |
| Egypt         | 70%               |
| India         | 59%               |
| South Africa  | 57%               |
| Indonesia     | 44%               |
| United States | 25%               |

: Mobile only internet usage

|    Contry   | Average RTT | downlink speed | uplink speed |
| ----------- | ----------- | -------------- | ------------ |
| South Korea | 278ms       | 1.8Mbps        | 723Kbps      |
| Vietnam     | 305ms       | 1.9Mbps        | 543Kbps      |
| US          | 344ms       | 1.6Mbps        | 658Kbps      |
| UK          | 372ms       | 1.4Mbps        | 782Kbps      |
| Russia      | 518ms       | 1.1Mbps        | 439Kbps      |
| India       | 654ms       | 1.2Mbps        | 633Kbps      |
| Nigeria     | 892ms       | 541Kbps        | 298Kbps      |

: Average RTT & downlink / uplink mobile speeds

## Summary
As we have seen in this chapter, decreasing latency is the best way to decrease page load times. Unfortunately, we have already reached the physical limits of our networking technologies, so we can't hope to improve latency by improving the physical layer. This means that all our efforts should be directed in creating protocols tailored for low latency.
