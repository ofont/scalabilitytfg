# Usability Engineering delays

[see @Ghemawat2003 p.93] ssays shit.

Book:

Usability Engineering, Jakob Nielsen, 1993

|    Delay    |     User reaction     |
| ----------- | --------------------- |
| 0 - 100ms   | Instant               |
| 100 - 300ms | Feels sluggish        |
| 300 - 100ms | Machine is working... |
| > 1s        | Mental context switch |
| > 10s       | I'll come back later  |

[SPDY and the Road Towards HTTP 2.0 video](http://www.youtube.com/watch?v=SWQdSEferz8)
250ms sweet spot load page time (Google, Yahoo, times)

Typical webpage (1MB):

|  Resource  | Avg # Requests |
| ---------- | -------------- |
| HTML       |              8 |
| Images     |             53 |
| Javascript |             14 |
| CSS        |              5 |

Avg RTT Time (latency): ~100ms worldwide

Mobile Avg latency: ~400ms (3G), ~150ms (4G)

When you reach 5Mbps, diminishing returns in HTTP performance.

Latency is very hard to remove. Basically bound by physics (speed of light)