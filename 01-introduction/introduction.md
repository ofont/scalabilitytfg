# Introduction {.unnumbered}
This project was born due my interest in web operations. Web operations is far from a well-defined field. It encompasses understanding of networks, routing, switching, firewalls, load balancing, high availability, disaster recovery, TCP, UDP, NOC management, hardware specifications, UNIX undersanding, web servers, caches, databases, storage infrastructure, cryptography, algorithms, trending and capacity planning (@Allspaw2010 p22). Since the field is so broad, and the number of problems to tackle so high, actually defining the scope of this project has not been easy.

The original purpose of the project was to pursue an investigation regarding scalability issues and patterns to provide solutions in that regard, but as I got deeper in the subject, the better I understood that scalability is a fickle issue, deeply tied to each platform's architecture.

Reading about the subject, I came across High Performance Websites, a book centered on front-end performance. I was surprised about his claims that focusing on the front-end was the best way to improve the speed of a website (as much as 80%!) (@Souders2007, p. xi).

Even though the subjects fundamentally tackle different problems: serving more users versus serving them faster, I found the second approach much more feasible (from a hardware point of view) and relevant to a wider range of products, because not everyone has a million users, but anyone can appreciate a faster service.

The project will try to answer the following questions:

- What causes slowness in web applications?
- What can we do about it?
- Are the changes worth it?

The project has been divided in 6 chapters:

On the first chapter, we will sketch the typical current web application and dissect it. This will help us get an idea of what has to be delivered to the user and why, or why not, the underlying pipes are optimized for this kind of traffic.

On the second one, we will analyze what kind of networking links our users have. The latency bottleneck will be introduced in this chapter.

On the third, we start dealing with the transport layer. How does TCP work, and what latency does it introduce to the connection will be answered here.

On the forth, we talk HTTP. I will do a quick tour on its evolution, on how it was supposed to be used, how it is used nowadays, and why those new use cases are really pushing the envelope. They are pushing it so hard, in fact, that a new version of HTTP (2.0) is on the works. We will also talk about that.

On the fifth we talk about browser rendering. How does the browser schedule the resources, how does it construct the render tree of a webpage and what can we do to help it work faster.

Finally, on our 6th chapter, we will perform and performance assessment on a real web application: LdShake (@Hernandez-Leo2011). This chapter will serve as a wrap-up to the previous ones and a demonstration that, indeed, front-end performance does improve our users' experience.

