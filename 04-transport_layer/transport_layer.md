# Transport Layer

In this section I will discuss the mechanisms that TCP uses and their effects on webpage load times. I will also propose some deployable fixes to increase the performance of this layer.
<!--
    Explain a bit of TCP and HTTP. How they interact and why it is not ideal. Explain multiple connections, slow start, keep-alive, connection-drops.
-->

TCP is the transport protocol over which HTTP works. TCP provides a virtual, reliable, end-to-end pipe between hosts processes. Since TCP works over an unreliable stack, it provides mechanisms for:

- retransmission of lost data
- in-order delivery
- congestion control and avoidance
- data integrity
- ...

TCP is very convenient for accurate delivery of data, but the failsafe mechanisms it uses, introduce dedlay. In this section, we will discuss them further, with special emphasis on their interaction with the typical HTTP flow.

## Three-Way handshake (3WHS)
Since a TCP stream is not stateless, it is necessary to perform a handshake between the client and the server before any data can be transmitted. It is also important to note that, since the application layer and the transport layer are independent, there is no way around this. The connection schema is as follows:

![Three-way TCP handshake, from @Grigorik2013, ch. 2](figure-02_05.png)

- client sends SYN, with random sequence number and optional TCP flags.
- server sends SYN ACK, with its own flags and options.
- client sends ACK. At this point application data can be sent already.

<!--
    Maybe explain more about the sequence numbers and why are they used?
-->

It is important to nore that before any data can be sent a full roundtrip has been spent. This roundtrip delay does not depend on bandwidth but on latency, and latency is basically capped by the length of the link and the speed of light.

So, to summarize, opening a connection has a variable RTT cost that must be taken into account, in the form of latency. We want to reuse TCP connections as much as possible (that is why HTTP1.1 introduced keep-alive by default, instead of connection per resource).

If we take a look to the handshake process under HTTPS (that is, HTTP over a TLS authentication layer), the delays are even more dramatic, since 2 additional RTT are needed (1 if the resumed version can be used).

![Three-way TLS handshake, from @Grigorik2013, ch. 4](figure-02_06.png)

The TLS needs one RTT for the server to send it's public certificate to the client. When the client received it, the pubkey is checked against it's CA, and if it is deemed valid (authentication step), the client will decide on a key to encrypt the communications (symmetric-key encryption). This key will be encrypted with the public key of the server and sent during the second RTT, along with the ¿possible encryption schemas? (AES, DES...). Finally, the server will accept some configuration, send an ACK and, after that, the data can be sent.


![Three-way TLS resumed handshake, from @Grigorik2013, ch. 4](figure-02_07.png)

If we have already established one encrypted connection with the server, we already hold a copy of its public key, so the first round of the communication is not needed. This is why browsers hold multiple HTTPS connections to the same host until the first one has been established, so they can prevent a roundtrip from happening.

Even if establishing a TLS connection is costly, it has many advantatges over an unencrypted connections. Apart from the security issue, since the data sent through HTTPS is encrypted and, thus, obscured, it is possible to use other protocols thant HTTP over TLS without being blocked by firewalls or proxies. We will discuss why this is convenient during our talk about HTTP 2.0 and SPDY.


### TCP Fast Open
This proposal enables the carrying of data within a SYN / SYN ACK packet, thus saving up to one full round trip time compared to standard TCP 3WHS. It was proposed by Google employees in 2011 (@Radhakrishnan2011), and its RFC is currently being developed at @Chu. It has been implemented in Linux kernel 3.7+, for both client and servers.

<!--
    Security problems associated with T/TCP.
http://www.mid-way.org/doc/ttcp-sec.txt, 1996.
[9] T/TCP Vulnerabilities. Phrack Magazine, 8(53), July 1998.
-->
Similar proposals have been made (see @Radhakrishnan2011) but they haven't gained much traction, required too many changes on the implementation (and socket API), introduced security issues or were more costly (computationally). Another similar alternative currently implemented by browser vendors is the *PRECONNECT* state, where the browser preemptively opens a TCP connection to the most visited sites by the user, avoiding the DNS resolution and TCP 3WHS. Since the probability that the user will visit one of those sites is high, the perceived performance of the browser will be better, and the drawbacks small (@Grigorika).

The current TCP implementation already allows sending SYN packets with data, but the server is forbid to reply to them until the connection has been established. This is so mainly for security purposes. If the server could reply to a request without a previous handshake it would be open to attacks such as:

SYN Flood / Server Resource Exhaustion
:   By sending SYN packets with requests, the server should also handle the load to compute the response, which may be quite high.

Amplified Reflection Attack
:   By spoofing the IP of the requests, the server would send the responses against any victim. This way, any server implementing TFO could be used as a part of a botnet by anyone.

To prevent this 2 issues, while allowing a fast TCP connection and keeping the changes to the TCP protocol to a minimum the following 2-phase protocol has been defined:

#### Requesting a TFO security cookie
1. The client sends a SYN packet with a Fast Open Cookie Request in the TCP option field.
2. The server generates a cookie bytestring by encrypting the client address with some symmetric-key cipher (currently an 8byte cookie, encrypted with AES-128). The server responds with a SYN-ACK that includes the generated cookie in the TCP option field.
3. The client can cache this cookie to Fast-Open connections to the server. It is also important to note that this procedure has established a valid TCP connection that may be used to send data (getting the cookie is not a performance drawback from standard TCP).

#### Using a TFO cookie
1. The client sends a SYN with a cached Fast Open cookie (as TCP option) along with the application data.
2. The server validates the cookie by decrypting it and matching it with the client IP address (or viceversa).
    a. If the cookie is valid: The server sends SYN-ACK that acknowledges the SYN and the data. The data is delivered to the server.
    b. If the cookie is not valid: The server sends a SYN-ACK that only acknowledges the SYN, not the data. The data is dropped. Connection will fallback to a regular 3WHS.
3. If the SYN data was accepted, the server may transmit additional response data segments to the client before receiving its first ACK.
4. The client sends an ACK acknowledging the server SYN. If the client's data was dropped, it is sent with the ACK (as a normal 3WHS connection would).
5. The connection has been established and proceeds as a normal TCP connection.

![TFO connection overview, from @Radhakrishnan2011](tfo_connection_overview.png)

The encrypted cookie is the main mechanism against the 2 previously detailed vulnerabilities.

SYN Flood / Server Resource Exhaustion
:   The server will not process any request with a foul or nonexistant cookie, but it must be noted that anyone could request a valid cookie. The proposed strategy to prevent resource exhaustion is to have a limited number of pending TFO requests, that is, connection requests that haven't been fully established. If the total number of TFO requests should exceed that number, the server would deactivate TFO and switch to 3WHS. The protocol is still vulnerable to SYN flooding, but not more than standard TCP is.

Amplified Reflection Attack
:   Since the cookie consists of the encrypted IP with a secret key (that has to be periodically revoked), it is not easy to spoof requests to overload a targeted victim. This attack can only take place if the key is compromised or the attacker steals a valid cookie from the target. The proposal also argues that if a system has been compromised to such an extent, there will be far more damaging ways to attack it.

## Congestion Avoidance and Control
In the early 80s, when the internet was still called ARPANET, the congestion collapse problem was observed for the first time. As stated by John Nagle (RFC 896, see @Nagle):

> Congestion control is a recognized problem in complex networks. We have discovered that the Department of Defense’s Internet Protocol (IP), a pure datagram protocol, and Transmission Control Protocol (TCP), a transport layer protocol, when used together, are subject to unusual congestion problems caused by interactions between the transport and datagram layers. In particular, IP gateways are vulnerable to a phenomenon we call **"congestion collapse"**, especially when such gateways connect **networks of widely different bandwidth**...

In those early times this was not much of a problem, since most nodes had uniform bandwidth and excess capacity, but that didn't hold true for long. This is the reason TCP has multiple built-in mechanisms to regulate the rate in which data can be sent:

- flow control
- congestion control
- congestion avoidance

We will explain those mechanisms further, paying special attention on the aspects that hurt HTTP performance the most.

### Flow Control
Flow control is a mechanism that prevents the sender from overwhelming the receiver by sending more data than it can handle. To control the data rate, each side advertises a receive window (*rwnd*) in it's ACK. This way, it can be dynamically adjusted based on the processing speed of the sender and the receiver. In a web stream, it is typically the client window the bottleneck factor.

### Slow Start
Flow control prevents the sender from overwhelming the processing power of the receiver, but it does nothing to prevent the overload of the underlying network. The available bandwidth of the link is unknown to both parties, so we need a mechanism to adapt the throughput of the connection to the available resources as fast as possible. This is where slow start comes into play.

Each sender has an internal congestion window size (*cwnd*) variable, that starts small and grows as long as packets are acknowledged. The initial value of the *cwnd* window is set by the operating system and has seen much debate over the years. In the original RFC for TCP (@Postel) it was set to one. Later one, in RFC 2581 (April 1999) (@Stevens), its value was increased to 4 and, recently, in RFC 6928 (April 2013) (@Chua) has been set to 10. The initial window size limits the effective bandwidth of links in short-lived connections (such as HTTP ones tend to be), so bigger limits yields better performance. In fact, the suggestion that the *cwnd* should be increased to 10 was noted in @Herbert2009. The paper compares latency using different cwnd, and discovers that a *cwnd* of 10 yields a +13% performance over a *cwnd* of 1 (against a +8% of a *cwnd* of 4).

Each time an ACK is received, the number of packets sent by the sender is doubled, so the *cwnd* window grows exponentially.

The slow start phase ends when *cwnd* goes over a OS fixed threshold called ssthresh or a packet is lost. In the first case, TCP enters congestion avoidance mode, while in the latter case it switches to fast recovery mode.

Slow-start is an important phase in web performance because it limits the available bandwidth of the link at the start of the connection, and since a lot of HTTP connections are short and bursty, they may never get to use the full link throughput.

![TCP Slow Start, from @Grigorik2013, ch. 2](congestion_control_avoidance.png)

### Slow Start Restart
This mechanism resets the *cwnd* of an idle TCP connection. The reasoning behind this behaviour is that the network conditions may have changed while the connection has been idle, so it is safer to start transmitting with a lower window. Since SSR has a negative impact on HTTP keep-alive connections, it is often disabled on servers:

    $> sysctl net.ipv4.tcp_slow_start_after_idle
    $> sysctl -w net.ipv4.tcp_slow_start_after_idle=0

Increasing the initial `cwnd` size it's an easy way to improve the performance of the server. Any Linux kernel above 2.6.39 already works with the new default value of 10. Kernel 3.2+ also has other updates like proportional rate reduction for TCP (congestion mechanism).

### Congestion Avoidance
If we continuously doubled the *cwnd*, we would soon be sending packets over the capacity of the network and start losing them. To avoid this undesirable behaviour, while slowly approaching the bandwidth limit of the network a new threshold, called Slow Start Threshold (*ssthresh*), is introduced. *ssthresh* is set to half the *cwnd* value when the connection last suffered a packet loss.

Once *ssthresh* is reached, the *cwnd* will increase linearly. Instead of doubling the window each RTT, *cwnd* will only increase by one. If a packet is lost or we receive a triple duplicated ACK, TCP enters Fast Recovery mode.

### Fast Recovery
This is the phase where most TCP implementation differ. The first implementation of TCP (Tahoe) didn't have any fast recovery mechanism and each loss triggered slow start. Shortly after, TCP Reno was introduced with a fast recovery mechanism, and every other TCP implementation since has followed that schema. The current implementations of TCP on Linux (CUBIC) (@Rhee) and Windows (Compound) (@Tan2006]) work in a similar fashion, but are much more aggressive, which enables them to get optimal performance from the link much faster. For a more in-depth comparison between current TCP implementations I recomend the paper *Performance Analysis of Modern TCP Variants* [@Symposium2010]. It does a very interesting comparison between CUBIC, Compound and New Reno and shows how fast and fair this implementations are.

Fast Recovery resends the lost packet and sets the *cwnd* to *cwnd / 2*. *ssthresh* is also set to *cwnd / 2*. From this point we enter congestion avoidance phase once again.

<!-- Check the last paragraph and put the images from Kurosa and a SawTooth TCP flow-->

### Head of line blocking
Since TCP ensures that the packets will be delivered in order, if a packet is lost, all subsequent packets must be held in a buffer until the lost packet has been recoverd. This introduces jitter, which may be fatal in certain kinds of applications, like video or audio streaming or online gaming. For this reason, TCP is not recommended for realtime applications.

## Summary
As we have seen, for typical web traffic, the delays in setting up a TCP connection are the main factor that hurt our performance from a transport layer perspective. To mitigate this we may:

- **Increase the initial congestion window** to 10, by upgrading the kernel.
- **Disabling Slow Start Restart**, so already established connections don't have to go through that initial phase.
- **Enable window scaling**, so the connection is not limited by *rwnd* but by *cwnd*.
- **Enable TCP Fast Open**, and data will be sent in the first rountrip.
- **Put servers closer**, so RTT are reduced. This usually means using a CDN to store resources.
- **Reuse established TCP connections**, by using keep-alive connections in HTTP.

Although TCP hasn't changed that much since its origin, updating our servers to the latest kernels is the best way to ensure our services are delivering the best transport layer performance possible.

## Chapter references
This chapter is based on @Grigorik2013, ch. 2. Congestion avoidance, fast recovery and head of line blocking have been expanded with @Kurose2010, ch. 3.
