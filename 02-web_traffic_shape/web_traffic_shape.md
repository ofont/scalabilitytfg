#Web Traffic Shape
<!--
    Explain here the typical shape of a web. How many requests, what kind of resources, short bursts vs long streams.
-->
In this initial chapter we will discuss the typical structure of a current webpage and the kind of resources it employs. It is very important that we are aware of what kind of traffic we must deliver to our clients to optimize our tools for it. We will also talk about the need for speed as a necessity to create compelling user experiences.

## The need for speed
In a world where fast-paced developments are the norm, it may seem that implementing features quickly is more important than implementing them efficiently. Still, as true as this may be, user interaction does make or break a lot of apps. The usability of an app may very well determine it's success and the willingness of a user to repeatedly use that service.

Speed and fast response times are one of the key aspects of usability engineering, as stated by Jacob Nielsen, one of the most well-regarded experts in that matter. From his Usability Engineering book (@Nia, ch. 5):

> The basic advice regarding response times has been about the same for thirty years (Miller [1968]; Card et al. [1991]):
> 
>   - **0.1 second** is about the limit for having the user feel that the **system is reacting instantaneously**, meaning that no special feedback is necessary except to display the result.
>   - **1.0 second** is about the limit for the **user's flow of thought** to stay **uninterrupted**, even though the user will notice the delay. Normally, no special feedback is necessary during delays of more than 0.1 but less than 1.0 second, but the user does lose the feeling of operating directly on the data.
>   - **10 seconds** is about the limit for **keeping the user's attention** focused on the dialogue. For longer delays, users will want to perform other tasks while waiting for the computer to finish, so they should be given feedback indicating when the computer expects to be done. Feedback during the delay is especially important if the response time is likely to be highly variable, since users will then not know what to expect.

From the citation we can infer that any interaction should finish in less than 0.1 to keep the app snappy and page loads should take about a second.

Faster pages generate better user engagement which, at the end of the day, translates to higher revenues. Therefore, if we think of speed as a feature, maybe skimping on it isn't worth it after all.

## A typical Website
[http://httparchive.org](http://httparchive.org) is a great resource of updated data about the current state of the web. It scans the top 10.000 ALEXA sites and analyzes how many resorces they employ, their size, how long do they take to load, and other intersting data. Apart from the data they showcase in their web, it is also possible to download it and make your own statistics and analysis.

![average webpage size, from http://httparchive.org](avg_bytes_per_page.png)

In figure 1.1 we can see the current average webpage (May 15 2013). It's roughly 1.5MB in size. Images acount for a 60% of the size, and scripts for another 20%. 

JPEG is the most popular image format, followed by PNG and GIF. Flash objects are the biggest in size (on average).

![average size per resource, from http://httparchive.org](avg_response_size.png)

![average webpage size vs average number of requests, from http://httparchive.org](avg_transfer_size_and_requests.png)

From this first 3 figures it is pretty clear that the current average webpage (as of May 15) is about 1.5MB in size, and is composed of 92 requests. The majority of its content is images (60+%) and Javascript.

Each individual resource also tends to have a rather small size. HTML, Javascript and CSS don't usually go over 15kB, and it is only images and flash content that makes heavy use of bandwidth.


![average resource size & number of requests, from http://httparchive.org](resource_transfer_size_and_requests.png)

We can see from the detailed view of transfer size per object that the amount of Javascript, HTML and CSS is roughly the same as it was a year before (stats from 15/04/12 to 15/05/13) but, while the number of image requests is also constant, the transfer size has steadily increased. This could be due to added support for new high density screens (tablets and laptops).

## Summary
In this chapter, we have taken a glimpse at the importance of interactivity in websites. From the data retrieved from *httparchive* we can also see that javascript is indeed very important for the actual web.

This reliance in javascript and technologies like AJAX allows us to create single-page websites more easily, allowing us to change its content dynamically.

The rise in interactivity, multimedia content and real time services puts a great strain in the current network stack, which was originally designed to transfer static content.

In the next chapters we will talk about how the network, the transport and the application layer delay the delivery of a webpage.
