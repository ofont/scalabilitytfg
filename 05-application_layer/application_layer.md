# Application Layer
In this chapter we will talk about the origins of HTTP (0.9 / 1.0), its current state (1.1), and the future of the protocol (2.0). We will make special emphasis on the current strategies to deliver high performance through HTTP 1.1 and how HTTP 2.0 will (and is) changing that.

## Evolution of HTTP
Let us start doing a quick summary to the milestones of the HTTP protocol.

### HTTP 0.9 (1989-1991)
HTTP was first proposed by Tim Berners-Lee, the inventor of the World Wide Web, in 1991. In the first version of the protocol the requests were as follows:

- `GET` command, followed by a resource identification. Additional keywords could be added after a question mark.
- Request is terminated by a carriage return.
- Request is ASCII string.

The format of the server responses:

- Response in HTTP or simple plain text.
- Response is an ASCII stream.
- `ISINDEX` tag indicates id of other valid resources.
- Connection is terminated after response is sent.

Other protocol characteristics:

- Stateless
- Idempotent
- Able to refer to other resources

As we can see, the core of what constitues HTTP has not changed much: it still is a protocol mainly targeted at transferring files and referring clients to other servers and resources.

More information about the implementation of HTTP 0.9 can be found in (@Wo) and (@W3.org).


### HTTP 1.0 (1991-1999)
As the WWW began to grow in numbers, the use cases for HTTP expanded and it soon became clear that the protocol would have to expand beyond the delivery of hypertext. The evolution from 0.9 to 1.0 was a very organic affair, developing from 1991 to 1995 in a rather anarchic way. It was in May 1996 that the RFC 1945 (@Berners-Lee1996) was published, describing the common usage of HTTP until now. The same RFC already states that it is no more than a memo:

> This memo provides information for the Internet community. This memo does not specify an Internet standard of any kind. Distribution of this memo is unlimited.

From the RFC we can summarize HTTP 1.0 as follows:

- Request may contain multiple lines, separating the headers.
- Responses have a header with status information.
- Responses not limited to HTML.
- Encoding of the response could be declared in the headers.
- Responses could be compressed.
- Authorization mechanisms were added.
- Caching mechanisms were added.
- Connection is terminated after response is sent.

With those changes, HTTP1.0 became a whole lot more useful than its predecessor 0.9, but we can still see 2 design flaws that will hurt efficient delivery of the content:

- **Connections are not reused**. This means, for every resource, having to to through the TCP 3WHS and Slow Start phases.
- **Headers are not compressed**. This one is big especially nowadays, when a lot of the data is sent through many small JSON requests. In those cases were the responses are very small, the overhead may be on the 80~90% range.


### HTTP 1.1 (1999-present)
Six months after the publication of the HTTP 1.0 RFC, the first draft of what would become HTTP 1.1, RFC 2068, was published (@Fielding). The final version of the protocol was delayed until 2 years later, in June 1999, as RFC 2616 (@Fieldinga).

HTTP 1.1 was not a huge departure in functionality from 1.0, but it added a lot of features to improve its performance, the main of which were:

- keep-alive connections by default.
- chunked encoding transfers.
- byte range requests.
- additional caching mechanisms.
- transfer encodings.
- request pipelining.
- client cookies.

### HTTP 2.0 (2014-onwards)
The seed of what will become HTTP 2.0 was planted in 2009, with the introduction of the SPDY protocol, developed at Google. SPDY is a protocol designed with minimal latency in mind, whose main technical goals are:

- **multiplexing** of many concurrent HTTP requests on a single TCP connection.
- **reduced overhead** by compressing headers.
- **simple and efficient parsing** of the message protocol.
- **TLS as underlaying transport protocol**, for better security and compatibility.
- **enable server initiated communications** (push) with the client whenever possible.

SPDY (and HTTP2.0) do not plan to replace the HTTP syntax, but to provide better ways to take advantage of the underlaying network. The results so far are promising: 39% - 55% speedup over HTTP, as stated by the introductory SPDY whitepaper (@Belsheb).

Even though the future is already here, with SPDY in its 3rd iteration, and deployments by Google (@\cite{M}), Twitter (@Raghav), Facebook (@KlintFinley) and other major players like Amazon (@Paul), it's adoption rate is very low (0.9% of all websites) (@Q-Success). Still, browser support is sizeable (54.19%) (@CanIUse) and increasing (@RafaelRi), which will hopefully convince more websites to make the switch.

## General optimizations
<!-- Steve Souders rules -->

## HTTP 1.1 in detail
It soon became clear that HTTP 1.0 was not flexible enough to deliver the increasingly diverse media of the WWW. The protocol had too much overhead (one TCP connection per resource), didn't support sending variable length transfers, or specific parts of a document, its cache mechanisms were primitive and there was no way to keep state of the clients. With all of this in mind, the 1.1 HTTP standard introduced:

- **keep-alive connections** to reuse established TCP connections for various HTTP requests.
- **chunked encoding transfers** to allow sending documents one piece at a time. This allows server to send documents with unknown length, which enables earlier flushing of dynamically generated content.
- **byte range requests** to allow partial transmissions of a resource. This is what allows pausing and restarting of downloads.
- **better caching and revalidation mechanisms** like the *Cache-Control*, *If-none-match* and *ETag* headers.
- **transfer encodings**. The tags *Accept-Encoding* and *Transfer-Encoding* allow both client and server to send the data in plaintext, compressed (*gzip*) or chunked.
- **request pipelining**, which enables sending multiple requests simultaneously.
- **client cookies**, which are included in the request headers and allow the server to keep track of the user session.

### Keep-alive connections
One of the most significant changes in HTTP 1.1 was the introduction of persistent connections. Until then, each resource had to initiate a connection, with its own handshake (+1 RTT) and go through the Slow-Start phase each time. This made sense originally, since webpages were hypertext only or the number of alternative resources was very low. Since this is not the case anymore, reusing TCP connections is a good way to make better use of the bandwidth.

![HTTP with no keep-alive, from @Grigorik2013, ch. 12](http-no-keepalive.png)

![HTTP with keep-alive, from @Grigorik2013, ch. 12](http-keepalive.png)

In this diagram we observe that the first case (sequential, no keep-alive) needs 284ms to transfer an HTML and CSS file. With keep-alive the number drops to 228ms. One less RTT. The savings can get much larger depending on the latency of the network and the congestion window of the server. As explained in the 4th chapter, small congestion windows on the server will make even small transfers like HTML files slower. In this case it is even more important to skip the Slow Start phase.

*Keep-alive* is so important that the feature was backported to HTTP 1.0 and it stands as one of the main reasons not to serve HTTP 1.0 requests.


### HTTP pipelining
In a typical HTTP flow, a request can only be sent after receiving a response. This is a suboptimal way to transfer files, since the server sits idle for a full RTT between requests. To solve this problem, HTTP pipelining was introduced. With HTTP pipelining it is possible to send consecutive requests before receiving a response.

![HTTP sequential pipelining, from @Grigorik2013, ch. 12](http-pipelining.png)

As we can see from the image, the server has a FIFO queue that processes each request sequentially and sends it to the client. We may even process the requests in parallel, like this:

![HTTP parallel pipelining, from @Grigorik2013, ch. 12](http-pipelining-parallel.png)

If we pay close atention to the diagram, it is clear that even if the CSS is processed before the HTML, it is *held* until the HTML has been sent. This problem receives the name of **head of line blocking**, and it may severely degrade the performance perceived by the client if any of the scheduled requests hangs or suffers unexpected delays of any kind, since all other resources must wait for it to finish. It may also cause performance problems for the server, that has to buffer the already-to-be-dispatched responses until the first one is out (may lead to resource exhaustion). Better scheduling of the requests is also impossible, since the client does not know the cost of each.

HTTP pipelining also causes problems with incompatible intermediaries, that may drop the connection or serialize it.

Since the implementation of HTTP pipelining has been riddled with issues it is not even enabled by default in browsers. 2 approaches to this issue exist: *implement multiplexing* (HTTP 2.0 way), which allows interleaving of multiple requests in the same connection, or *allow multiple TCP connections*, which brings us to the next section.

### Multiple TCP connections
Pipelining was a great idea (performance wise) poorly implemented so, to get a similar speedup, the HTTP standard allowed multiple parallel TCP connections per host.

Opening multiple TCP connections per page adds more complexity to the client, that has to deal with multiple sockets. It also consumes more CPU and memory resources, jeopardizes TCP fairness mechanisms and hurts battery life.

The HTTP 1.1 RFC says to limit persistent connections to 2 per server.

Extracted from the RFC 2616 (@Fielding, section 8.1.4):

> Clients that use persistent connections SHOULD limit the number of simultaneous connections that they maintain to a given server. A single-user client **SHOULD NOT maintain more than 2 connections with any server** or proxy. A proxy SHOULD use up to 2*N connections to another server or proxy, where N is the number of simultaneously active users. These guidelines are intended to improve HTTP response times and avoid congestion.

In practice, though, any browser will allow a maximum of 6 TCP connections. This magic number of 6 has been growing throughout the years and is basically a compromise between the overhead of establishing new connections and the degree of parallelism. Six connections tends to work well enough for most sites, but this is not the case for every site. In those cases, resources are served from multiple domains or subdomains, a technique that received the name of *domain sharding*.

### Domain sharding
If 6 parallel connections are not enough, resources may be split into multiple domains. This forces a new DNS resolution on the client (more delay) and makes the site harder to manage to the admin, but may yield performance improvements, especially on heavy sites.

I have performed some tests on the flickr explore page (http://www.flickr.com/explore) to show how sharding is employed by a heavy website:

![Flickr domain sharding](flickr_domain_sharding.png)

We can see from the chrome network inspector that images are downloaded from multiple domains, called `farmX.staticflickr.com`. Now we perform some dns lookups on those farms, to see where they resolve:

    $ nslookup farm3.staticflickr.com
    ...
    Address: 77.238.160.184

    $ nslookup farm4.staticflickr.com
    ...
    Address: 77.238.160.184

    $ nslookup farm6.staticflickr.com
    ...
    Address: 77.238.160.184

    $ nslookup farm8.staticflickr.com
    ...
    Address: 77.238.160.184

    $ nslookup farm9.staticflickr.com
    ...
    Address: 77.238.160.184

It is very clear now that the domains are only aliases to the same server. In fact, the domain is also behind a CDN. If we try to lookup the address from the United States, for example, it resolves to a different ip:

    $ nslookup farm6.staticflickr.com
    ...
    Address:    64.90.63.203

Different cnames are used to download a larger stream of images from the same server in parallel. In this particular example, employing this technique makes a lot of sense, since flickr explore is an infinite-scrolling page with photographs. With so many images to serve, more connections will help to lessen the load times of the page and improve the user experience. The usage of a CDN to distribute the content also brings us to the next point.

#### CDN
If we do not want to deal with sharding ourselves, or we are interested in lowering the latency for our users, using a CDN to deliver our static resources is a popular and well proven choice.

A CDN will store and distribute the content we feed them all around the world, lowering the latency median. You won't have to take care of sharding or caching, and traffic spikes will be more manageable, since we are only dealing with the generation of the dynamic content.

CDNs are very widely used nowadays, and have become increasingly necessary to deal with the popularity of multimedia streaming sites, like YouTube.


### Caching and validation mechanisms

Having a fast fresh view of our applications is important, especially for new users, but existing users can get even better performance of a web application, if properly configured. HTTP cache mechanisms provides us tools to store resources in the client, which can decrease greatly the number of requests to our server, making the web load faster and offloading our servers.

In order of priority, those are the ways a server can specify the freshness of a resource:

- Cache-Control: no-store header.
- Cache-Control: no-cache header.
- Cache-Control: must-revalidate.
- Cache-Control: max-age.
- Expires header.
- No cache header. Let the cache determine by itself.

#### Cache-Control: no-store, no-cache and must-revalidate
no-store
:   A response marked as *no-store* can't be copied to the cache and must be forwarded directly to the client. The purpose of *no-store* is to prevent sensitive data to be leaked through cache tampering. Since some cache implementations may ignore the *no-cache* header, it may be necessary to enable *no-store* to ensure that the content is fresh.

no-cache
:   A response marked as *no-cache* may be copied to the cache, but must be revalidated with the server each times it is requested.

must-revalidate
:   Some caches are configured to serve stale (expired) content. The *must-revalidate* header forces those caches to recheck with the server whether the resource is fresh or not.

#### Cache-Control: max-age and Expires
max-age
:   Seconds until the resource is considered stale. *max-age* uses relative time measures and is preferred over *Expires*.

Expires
:   Absolute expiration date of the resource. Depends on the computer clock set correctly, so it is not as reliable as *max-age*.

#### Heuristic expiration
If no cache is specified, the client may try some heuristics to determine the expiry date of the resource. The *LM-Factor* algorithm is a pretty extendede approach:

- calculate the time between the request and the *Last-modified* header of the resource.
- Multiply this delta by some factor, like 0.2.
- The result is the estimated *max-age* of the resource.

To prevent the heuristic to grow too large, usually upper bounds are placed (a day or a week are popular choices). Still, since this kind of approaches are not enforced nor standard, they should not be relied upon. This is why cache-headers should always be specified, to guard ourselves from erratic client behaviours.

#### Validation headers
An expired document does not necessarily mean it's gone stale, it means we can't assure it's fresh. To avoid requesting the resource anew HTTP can use conditional methods. Conditional methods allow the server to return a `304 Not Modified` response to the client, instead of the whole resource, which leads to a reduction in transfer times and server load.

The 2 conditional headers used in cache revalidation are:

If-Modified-Since \<date\>
:   Perform GET if resource has been modified since the specified date.

![Request with If-Modified-Since headers, from @Gourley2002, ch. 7](If-Modified-Since_example.png)

If-None-Match \<tag\>
:   Perform GET if resource *ETag* does not match the ones in the header. If not, return `304 Not Modified` response. *Etags* are labels attached to a resource with a unique identifier to represent the resource contents.

Usually, If-Modified-Since revalidation is good enough, but there are some cases when it falls short:

- Rewritten resources that contain the same data. Even if the content is the same, the modification date on the filesystem has been changed.
- Minor changes that do not warrant propagation.
- Resources with sub-second update intervals (like realtime monitoring). *IMS* has a 1 second granularity, so it could not be used in those cases.

To overcome these problems, HTTP 1.1 introduced Entity Tags (*ETags*). *Etags* are arbitrary labels to identify versions of a resource and take precedence over *Last-Modified* headers. It is very important to configure *Etags* correctly if requests are handled by a cluster. Apache and IIS webservers use an *ETag* format that is system dependant, so the same cluster may very well have different *Etags* for the exact same resource. To avoid this undesirable behaviour, *Etags* should be either manually configured or removed entirely.

![Request with If-None-Match headers, from @Gourley2002, ch. 7](If-None-Match_example.png)

<!-- 
, form @Gourley2002, ch. 7
and Souders. -->

### Resource compression
HTML does not allow header compression, but it certainly enables mechanisms to compress its payload. Resource compression is widely deployed for both clients and servers, fairly light on CPU resources and a highly effective way to reduce the transmitted byte count.

To manage encoding negotiation, HTTP clients inform the server with the header `Accept-Encoding`, with a list of available compression methods.

    Accept-Encoding: gzip, deflate

A server returning a compressed response, will indicate the algorithm in its `Content-Encoding` header:

    Content-Encoding: gzip

Not all resources benefit equally from being compressed though. Multimedia content has its own set of specialized compression algorithms and recompressing it with gzip (or any other general purpose algorithm) may even lead to bigger filesizes. This is why only text based content (HTML, CSS, Javascript and plaintext) should be compressed. Multimedia file format may be a key decision for some web applications to keep its bandwidth expenditure at bay, although the winners nowadays seem clear: mp3 or AAC for audio, jpeg and png for images and h.264 for video. Still, there are emerging alternatives like WebP (@Google) for images or VP8 (@Bankoski2011) for video. In the video side, the successor of h.264, HEVC, has recently been published (June 7th) as a final standard (@Sector2013), so it is reasonable to expect a migration to this new, more efficient (@Ohm2012) (+35%) standard in the following years.

Multimedia content aside, gzip compression (most widely supported format) usually yields 70% reductions in file size so there is no reason not to use it.

| File type  | Uncompressed size |  Gzip size   | Savings |
| :--------- | ----------------: | -----------: | ------: |
| Script     |       3.277 bytes |   1076 bytes |     67% |
| Script     |      39.713 bytes | 14.488 bytes |     64% |
| Stylesheet |         968 bytes |    426 bytes |     56% |
| Stylesheet |      14.122 bytes |  3.748 bytes |     73% |


### Stylesheet and Script minification
Even though it may seem like the road to axing bytes is over after the compression step, we can still do better. Both javascript and css files tend to include a lot of white space, comments and other *unnecessary* characters that may be stripped without changing its functionality.

There are minification tools that perform this process automatically, so the development version can be as clean and readable as possible while keeping the production version size at a minimum. The Google Closure compiler (https://developers.google.com/closure/compiler/) or UglifyJS (https://github.com/mishoo/UglifyJS2) are popular choices for javascript minification, while the YUI Compressor (http://yui.github.io/yuicompressor/) seems to be the tool of choice for CSS minification.

I have performed some minification and compression test on the jquery to toss some data over which kind of size reduction might be expected:

|    Compression     |      Size     | Savings |
| ------------------ | ------------- | ------- |
| RAW                | 274.080 bytes | 0%      |
| minified           | 93.064 bytes  | 66%     |
| gzipped            | 81.352 bytes  | 70%     |
| minified + gzipped | 32.812 bytes  | 88%     |

As we can see, minifying can be as powerful as compressing. Moreover, the 2 techniques stack very well, trimming almost 90% of the original byte count. With smaller scripts, results probably won't be as good, but it is safe to assume an average reduction of an 80% in size, once both steps are applied.

### Concatenation and spriting

To go further in the road of reducing HTTP requests and overhead, we may consider resource concatenation. Scripts and stylesheets are executed sequentially as they appear in the HTML, so it is possible to combine them into less files.

If our web application is very dependant on javascript, it is possible that our pages use many script resources, be it external libraries or internal. As developers, it may be useful to develop our own libraries and functions in different files, since it makes the code more reusable and easier to refactor, but that does not mesh well with performance. Since it is especially important to deliver stylesheets and scripts for a fast rendering time (as we will see in the next chapter).

Deciding which scripts should be stitched together can be done with a dependency map. If we know, for every view on the webapp, which scripts are used, we can concatenate those that only used together in that view, while keeping scripts used in other *bundles* separate, for better cache usage.

The case of stylesheets is easier. Since the additions between different site views should be minor, and CSS retrieving is very important for progressive loading, it is better to have only one css file, even if some parts of the CSS are not used at first. This way, we can cache it for every other view right away.

Spriting consists in stitching images together, and use CSS to display the desired part of this new image sprite. Stitching images together is a very good option for pages that use a lot of images in combination with their CSS to increase the visual attractiveness of the site without having to rely on lesser supported CSS options. Those image resources may be very abundant and very small, so they would delay the page load (loads of small resources will be capped by RTT). Services like SpriteMe (http://spriteme.org/) detect the spriting opportunities of the page and offer a fairly automated way to generate and integrate those sprites on any website.

### Resource inlining
In the case of dealing with small resources, like small images or scripts, we might even consider embedding them in the HTML code itself. This is best saved only for small, rather static, content.

To inline binary resource, the src attribute may be a base64 representation of the object, instead of an uri. The format is as follows: `data:[mediatype][;base64],data`

    <img src="data:image/gif;base64,R0lGODlhAQABAIAAAAA
              AAAAAACH5BAAAAAAALAAAAAABAAEAAAICTAEAOw=="
        alt="1x1 transparent (GIF) pixel" />

Since base64 encoding adds a 33% byte overhead, and some browsers even have hard limits on the size those inline resources can reach, it is better to keep them under 1 or 2 kB.

In general, those 4 points are considered good criteria to decide for or against inlining:

- If the files are small, and limited to specific pages, consider inlining.
- If the small files are frequently reused across pages, consider bundling.
- If the small files have high update frequency, keep them separate.
- Minimize the protocol overhead by reducing the size of HTTP cookies.

### HTTP 1.1 performance rules
From what we have discussed until this point, the following rules will serve as guideline to improve our HTTP 1.1 applications:

1. **Use fewer HTTP requests**. Combine resources, inline them, use sprites or simply delete them. The less requests to perform, the faster it will load.
2. **Use a CDN**. Static content should be hosted as close to the client as possible. A CDN is a good way to distribute content and user perceived latency.
3. **Configure caching**. Add caching headers for all your resources. Use long cache deltas (like +10 years) and change the resource file name if a new version has to be deployed (to avoid stale cache issues).
4. **Compress the payload**. Use gzip for compressing text. Change the format or optimize the compression of your multimedia resources and minify both javascript and CSS.
5. **Avoid redirects**. Redirects add an additional RTT per request, so they should be avoided at all costs, especially for key resources.


### Headers and cookies
HTTP does not compress headers, so they may represent a lot of overhead in a request. Moreover, since HTTP is stateless, information like cookies must be transmitted on every request, which is bad. A way around this is to use another subdomain for static content. Requests to a new subdomain won't carry cookie data.

If we are using AJAX, with a lot of short json responses that don't need authentication this may be a very good approach to save some bytes too, since in those kind of short and frequent responses the protocol overhead is around 80%, and burdening it with cookies will only make it worse.


## HTTP 2.0 in detail
Now that we have come this far, it is rather clear that many of the workarounds needed to decrease the latency of existing HTML 1.1 deployments are rather inconvenient to deploy. Sharding, concatenation, resource inlining or spriting are techniques that seek better usage of the underlying TCP socket or faster downloads through parallelism. The latest HTTP 2.0 draft (@Belshe) provides an answer to most of the HTTP 1.1 woes by providing a new messaging frame, while maintaining the semantics of the protocol.

HTTP 2.0 won't change the semantics of the protocol, only it's framing. This will allow our existing applications to take advantage of the HTTP 2.0 performance boost (50% reduction in page load time) (@Be) as soon as our clients and servers are HTTP 2.0 compatible.

On this chapter I will explain HTTP 2.0 based on the latest available draft (@Belshe). This version is very similar to the SPDYv3 implementation, since it served as the core of the new specification, but I expect that this content will be soon outdated by new drafts. Still, the core ideas will remain, and they are interesting enough to be introduced.

### Overview
One of the main bottlenecks of HTTP 1.1 is the reliance in multiple connections for concurrent downloads. HTTP 1.1 had pipelining mechanisms that have been impossible to deploy due to intermediary interferences. Even if those middle boxes were a non-issue, pipelining still would suffer of *head-of-line*  blocking. To prevent this and other problems, HTTP 2.0 adds a framing layer that allows multiplexing of streams over the same TCP connection.

The main 4 improvements over HTTP 1.1 are:

Multiplexed requests
:   A single HTTP connection can handle any number of concurrent requests.
Prioritized requests
:   Clients can request certain resources to be delivered first. This would allow critical resources to be delivered first without congestion problems caused by other, less important, requests.
Compressed headers
:   Since HTTP headers send a significant amount of redundant data, compressing it will greatly reduce the overhead of the protocol.
Server pushed streams
:    Servers may deliver content to clients without an explicit request on their behalf.

### Layer abstraction
The new framing mechanism makes it necessary to introduce the following terminology in order to fully describe the data exchange in the new protocol.

Session
:   Synonym for connection. Consists of a single TCP connection.
Stream
:   A bi-directonal stream within a session. May be a typical request-response pair or a push stream.
Message
:   Sequence of frames that compound a logical HTTP message.
Frame
:   Smallest unit of communication. May contain control data or payload. Each frame has an identifier to associate itself to a stream.

![HTTP 2.0 layer abstraction, from @Grigorik2013, ch. 12](http2-layers.png)

### Binary framing
Instead of sending HTTP headers delimited by CRLF characters, HTTP 2.0 uses a well defined binary frame, similar to a TCP segment or an IP packet.

#### Frame header
    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |         Length (16)           |   Type (8)    |   Flags (8)   |
    +-+-------------+---------------+-------------------------------+
    |R|                 Stream Identifier (31)                      |
    +-+-------------------------------------------------------------+
    |                     Frame Data (0...)                       ...
    +---------------------------------------------------------------+

Length
:   16bit unsigned int indicating the length of the *frame data*.
Type
:   8bit type of the frame. Determines how the frame will be interpreted. Right now, there are 9 frame types defined.
Flags
:   8 bits reserved for frame related boolean flags.
R
:   1 bit reserved for future iterations of the protocol.
Stream identifier
:   31bit identifier that determines which stream is related to the frame. Session wide messages have the 0 identifier.
Frame data
:   Dependant on the frame type.

#### Frame types
`DATA`
:   Used to send payload. Each request or response may be sent using multiple `DATA` frames.
`HEADERS + PRIORITY`
:   Allows the sender to communicate the priority of the stream to the receiver and the headers of the request. Requests by the client will be sent using `HEADERS + PRIORITY` frames, while responses will be sent with a `HEADERS` frame.
`RST_STREAM`
:   Allows termination of a stream.
`SETTINGS`
:   Conveys configuration parameters related to the session as a whole. Must be sent at the start of a connection.
`PUSH_PROMISE`
:   Notifies an endpoint in advance thaht a certain resource will be sent in response to a certain request.
`PING`
:   Built-in mechanism to determine the RTT between endpoints.
`GOAWAY`
:   Gracefully kill a session.
`HEADERS`
:   Provide any number of header fields for a stream.
`WINDOW_UPDATE`
:   Built-in mechanism to regulate a session's flow control.

### Multiplexing
Since each frame contains information regarding the stream it belongs to, and TCP ensures in-order delivery of the packages it is possible to multiplex multiple requests on the same TCP stream. This is the single most important feature of HTTP 2.0, and the most critical one performance wise.

True multiplexing of streams enables:

- paralell requests and responses without HTTP *head-of-line* blocking.
- optimal TCP socket usage. HTTP sessions are now a mixed stream of data instead of bursts of requests and responses.

![HTTP 2.0 multiplexing of streams, from @Grigorik2013, ch. 12](http2-multiplexing.png)

The fact that all the requests can be transfered over the same connection makes most of the HTTP 1.1 performance strategies obsolete. We no longer need to distribute the content over multiple domains or reduce the number of resources by inlining them.

Lowering the number of required TCP connections per server, makes it possible to keep them open for longer periods of time (the spec declares that only the server should close the connection, and that it should remain open for as long as possible.). From @Belshe, section 3.1:

 > HTTP/2.0 connections are **persistent**.  That is, for best performance, it is expected a **clients will not close connections** until it is determined that no further communication with a server is necessary (for example, when a user navigates away from a particular web page), or until the server closes the connection. **Servers are encouraged to maintain open connections for as long as possible**, but are permitted to terminate idle connections if necessary.  When either endpoint chooses to close the transport-level TCP connection, the terminating endpoint MUST first send a GOAWAY (Section 3.8.7) frame so that both endpoints can reliably determine whether previously sent frames have been processed and gracefully complete or terminate any necessary remaining tasks.

### Prioritization
With multiplexing, we have much more data to stuff into the TCP stream, but if we do so arbitrarily it may end up being counterproductive. Bad frame scheduling may cause delays in the delivery of key resources and, to prevent that, HTTP 2.0 allows stream priorization.

The client can establish the priority of a stream at the start of a request, by sending a `HEADERS+PRIORITY` frame. Priority is represented as an unsigned 31 bit integer. 0 represents highest priority, while 2^31-1 the lowest one.

It should be noted that the draft does not specify any algorithm for dealing with priorities. This is left to the endpoint implementations.

### Header compression
Name/Value pairs in `HEADERS` and `HEADERS+PRIORITY` frames are compressed, although the method is still being discussed. The SPDYv3 implementation uses zlib with a custom dictionary encoding (@Yang), but since a vulnerability that allowed session hijacking was discovered in late 2012 (see @Hoffman) a replacement has yet to be found.

### Server push
Push transactions enable HTTP 2.0 to send multiple replies to a client for a single request. This is, in a way, an evolution to the inlining mechanism in HTTP 1.1. Traditionally, the server sends the requestes HTML to the client and it is the client that must discover and request every linked resource in the page. This communication schema introduces delay, since most of the resources have to wait a full RTT before they can be requested. With push transactions, the resource discovery is left at the server side, allowing it to send resources that the client is likely to discover.

To prevent race conditions (resources to be pushed are discovered and requested by the client, before being sent) HTTP 2.0 introduces the `PUSH_PROMISE` frame, which announces to the client which resources will be pushed in response to his request. The client has to explicitly deny `PUSH_PROMISE` it does not want to receive by issuing a stream error.

The push transaction workflow is as follows:

1. Client creates a new stream and send a request to the server
2. Server sends `PUSH_PROMISES` for the related content, that are spawned in new streams. Those promises include the URI of the resources and validation headers to determine whether or not the client has the resource in cache.
3. Server sends requested resource.
4. Client receives `PUSH_PROMISES`:
    a. Do nothing. Accept the push.
    b. Issue `CANCEL` stream error. Deny the push. May be due to the resource being already cached or `PUSH_PROMISE` pointing to a cross site URI (security concern).
5. Server sends promised data. May cancel transaction if it receives a `CANCEL` error before sending the resource.

![Single request instantiates multiple response streams, from @Grigorik2013, ch. 12](http2-serverpush.png)

### Deployment
Even if client and server can speak HTTP 2.0, we need a method to convey this information to the other endpoint and upgrade the connection. Nowadays, the most reliable and efficient method to do so is to employ the *Application Layer Protocol Negotiation* (ALPN) implemented in the TLS layer.

Since network intermediaries may tamper unencrypted traffic, most new application protocols are deployed using a TLS layer. Encrypting the traffic obscures the data and the protocol at the same time, so it is the most convenient way to avoid interferences from the network environment.

ALPN is able to negotiate the protocol upgrade using the TLS handshake, so there is no additional RTT (in contrast to a standard HTTP upgrade mechanism). The negotiation process is as follows:

- The client appends a `ProtocolNameList` field witha list of supported protocols into the `ClientHello` message.
- The server responds with a `ServerHello` message, containing a `ProtocolNameList` with the selected protocol.

The server must select only one protocol. If the server does not suport any of the protocols proposed by the client, the connection is closed.

![TLS handshake, from @Grigorik2013, ch. 4](tls_handshake.png)

### HTTP 2.0 performance rules
Most of the performance recommendations for HTTP 1.1 are no longer needed in HTTP 2.0:

1. **Use fewer HTTP requests**. Since all requests are multiplexed and the headers compressed, more requests will have less of an impact in the overall performance of the webpage.
2. **Use a CDN**. A CDN is still a good way to cut on latency. Still, techniques like domain sharding are no longer necessary, since HTTP 2.0 allows multiplexing.
3. **Configure caching**. Caching is as important in HTTP 2.0 as it was in 1.1. In that regard, the same mechanisms apply to both protocols.
4. **Compress the payload**. 2.0 allows the same mechanisms to compress the payload, but also compresses the headers, making small requests (like AJAX updates calls) have much less overhead. CSS spriting and javascript concatenation is no longer necessary, since requests are much cheaper in comparison to 1.1.
5. **Avoid redirects**. This still applies. Redirects should be avoided, since they add an RTT per request.

HTTP 2.0 also gives us more tools that will decrease the latency of the protocol, like the ability to push resources to the clients. Other interesting characteristics, like making some headers valid session or stream wide, are also being discussed into the protocol. This would make HTTP 2.0 a stateful protocol and allow us to send information like cookies or user agents only once per connection.


## Summary
As we have seen in this chapter, HTTP was originally designed to deliver static pages with few resources. Even though HTTP 1.1 is a major evolution of the original protocol, it got some key features wrong (pipelining) and has grown old for our current needs.

There are many workarounds around the limitations of the 1.1 protocol, but they encumber the development of web applications and don't solve the fundamental problems behind the protocol.

HTTP 2.0 is the solution to most of 1.1 woes, adding true multiplexing, push and prioritization. In table 4.3 I have made a comparison between the key features of both protocols.


|                    |        HTTP 1.1       |        HTTP 2.0       |
| -----------------: | --------------------- | --------------------- |
|      **Transport** | Keep-alive            | Keep-alive            |
|    **Concurrency** | Multiple connections  | Multiplexing          |
|        **Caching** | Cache-control headers | Cache-control headers |
|                    | Validation headers    | Validation headers    |
|    **Compression** | Content               | Content               |
|                    |                       | Headers               |
|           **Push** | No                    | Yes                   |
| **Prioritization** | No                    | Yes                   |

: HTTP 1.1 and 2.0 comparison

## Chapter references
This chapter is based on @Grigorik2013, ch. 12. The caching section has been expanded using @Gourley2002, ch. 7 and @Souders2007 ch. 13. For a more complete version of the HTTP 1.1 performance rules I recommend @Souders2007 and @Souders2009. The HTTP 2.0 section is basically a summary of the current specification draft (@Belshe).

